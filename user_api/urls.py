# urls.py
from django.urls import path
from . import views

urlpatterns = [
    path('api/signup/', views.signup_view, name='signup'),
    path('api/signin/', views.signin_view, name='signin'),
    path('api/logout/', views.logout_view, name='logout'),
    path('api/authenticated/', views.check_user, name='check_user'),
]
