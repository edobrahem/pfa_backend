# views.py
from django.contrib.auth import login, authenticate, logout
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
import json
from .serializer import UserSerializer
from django.contrib.auth.decorators import login_required
from shopping_cart.models import ShoppingCart

@csrf_exempt
def signup_view(request):
    if request.method == 'POST':
        
        data = json.loads(request.body)
        username = data.get('username')
        email = data.get('email')
        password = data.get('password')

        if not username or not email or not password:
            return JsonResponse({'error': 'Username, email, and password are required'}, status=400)
        user = User.objects.create_user(username=username, email=email, password=password)
        user.save()
        ShoppingCart.objects.create(user=user)

        return JsonResponse({'message': 'User signed up successfully'})
    else:
        return JsonResponse({'error': 'Invalid request method'}, status=405)

def signin_view(request):
    if request.method == 'POST':

        data = json.loads(request.body)
        username = data.get('username')
        password = data.get('password')

        if not username or not password:
            return JsonResponse({'error': 'Username and password are required'}, status=400)
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            user_data = UserSerializer(user).data
            user_data['user_id']=user.id
            return JsonResponse({'message': 'Login successful', 'user': user_data})
        else:
            return JsonResponse({'message': 'Login failed'}, status=401)
    else:
        return JsonResponse({'error': 'Invalid request method'}, status=405)


def logout_view(request):
    logout(request)
    return JsonResponse({'message': 'Logout successful'})

# @login_required
def check_user(request):
    if request.user.is_authenticated:
        return JsonResponse({'message': 'User is logged in', 'user': UserSerializer(request.user).data})
    else:
        return JsonResponse({'message': 'User is not logged in'})
