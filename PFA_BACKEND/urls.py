from django.contrib import admin
from django.urls import path,include
import product.views as product_views  


urlpatterns = [
    path('admin/', admin.site.urls),
   

    # path('user/',include('django.contrib.auth.urls')),  # Include the auth urls
    path('products/', include('product.urls')), # product urls
    path('orders/', include('orders.urls')),
    path('user/',include('django.contrib.auth.urls')),  # auth urls
    path('user/',include('user_api.urls')),

    path('cart/', include('shopping_cart.urls')),   # cart urls
]
