from rest_framework import serializers
from .models import Product, Review
from django.contrib.auth.models import User
from user_api.serializer import UserSerializer

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

class ReviewSerializer(serializers.ModelSerializer):
    user_details = UserSerializer(source='user')  # Assuming the review model has a 'user' field

    class Meta:
        model = Review
        fields = '__all__'  # Include all other fields of the review model plus the user_details