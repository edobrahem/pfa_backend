import random
import string
from django.db import models
from django.contrib.auth.models import User


def generate_unique_product_id():
    length = 8
    chars = string.ascii_uppercase + string.digits
    while True:
        product_id = ''.join(random.choice(chars) for _ in range(length))
        if not Product.objects.filter(product_id=product_id).exists():
            return product_id

class Product(models.Model):
    product_name=models.CharField(max_length=50, blank=True, null=True)
    product_id=models.CharField(max_length=8, unique=True, default=generate_unique_product_id)
    product_photo=models.CharField(max_length=500, blank=True, null=True)
    product_brand=models.CharField(max_length=50, blank=True, null=True )
    product_price=models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    product_description=models.TextField( blank=True, null=True)
    product_category=models.CharField(max_length=20, blank=True, null=True)
    product_quantity=models.IntegerField( blank=True, null=True)
    product_model_id=models.CharField(max_length=20, blank=True, null=True)

    def __str__(self): 
        return self.product_name
    
class Review(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='reviews')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.PositiveIntegerField(default=1)  # Rating out of 5, for example
    comment = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Review by {self.user} on {self.product}'
