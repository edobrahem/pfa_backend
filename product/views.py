import json
import os
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from .models import Product, Review
from django.utils import timezone
from .serializer import ReviewSerializer
from django.contrib.auth.models import User
from product.serializer import ProductSerializer

def create_product(request):
    if request.method=='POST':
        data = json.loads(request.body)
        product_name = data.get('product_name')
        product_brand = data.get('product_brand')
        product_price = data.get('product_price')
        product_description = data.get('product_description') 
        product_category = data.get('product_category')
        product_quantity = data.get('product_quantity')
        product_model_id = data.get('product_model_id')
        product_photo = data.get('product_photo')

        product=Product.objects.create(
            product_name=product_name,
            product_brand=product_brand,
            product_price=product_price,
            product_description=product_description, 
            product_category=product_category,
            product_quantity=product_quantity,
            product_model_id=product_model_id,
            product_photo=product_photo
        )
        product.save()
        return JsonResponse({'message':'Product created successfully!', 'product':serialize(product)})
    else:
        return JsonResponse({'message':'Wrong Method'})

def get_all_products(request):
    print(request.user)
    products=Product.objects.all()
    return JsonResponse({
    'products': [
        {**serialize(product), 'reviews_average': get_review_average(product.id)} 
        for product in products
    ]
})

def get_product_detail(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    reviews = product.reviews.all()
    serialized_reviews = [ReviewSerializer(review).data for review in reviews]  # Assuming ReviewSerializer returns a serializable object with a .data attribute
    reviews_average = get_review_average(product.id)
    return JsonResponse({
        'product': {
            **serialize(product),
            'reviews_average': reviews_average,
            'reviews': serialized_reviews
        }
    })

def get_products_by_category(request, category):
    products=Product.objects.filter(product_category=category)
    return JsonResponse({'products':[serialize(product) for product in products]})


def update_product(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    if request.method == 'PUT':
        data = json.loads(request.body)
        product.product_name = data.get('product_name', product.product_name)
        product.product_brand = data.get('product_brand', product.product_brand)
        product.product_price = data.get('product_price', product.product_price)
        product.product_description = data.get('product_description', product.product_description)
        product.product_category = data.get('product_category', product.product_category)
        product.product_quantity = data.get('product_quantity', product.product_quantity)
        product.product_model_id = data.get('product_model_id', product.product_model_id)
        product.product_photo = data.get('product_photo', product.product_photo)
        
        product.save()
        return JsonResponse({'message': 'Product updated successfully!', 'product': serialize(product)})
    else:
        return JsonResponse({'message': 'Method not allowed'})
    
def delete_product(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    if request.method == 'DELETE':
        product.delete()
        return JsonResponse({'message': 'Product deleted successfully!'})
    else:
        return JsonResponse({'message': 'Method not allowed'})
    
def serialize(self):
    return {
        'id': self.id,  
        'product_name': self.product_name,
        'product_id': self.product_id,
        'product_photo': self.product_photo,
        'product_brand': self.product_brand,
        'product_price': str(self.product_price),  # converted price to string for better usability in json 
        'product_description': self.product_description,
        'product_category': self.product_category,
        'product_quantity': self.product_quantity,
        'product_model_id': self.product_model_id,
    }



# REVIEWS 
def add_review(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            try:
                data = json.loads(request.body)
                product_id = data.get('product_id')
                rating = data.get('rating', 1)
                comment = data.get('comment','')

                product = Product.objects.get(id=product_id)

                review = Review.objects.create(
                    product=product,
                    user=request.user,
                    rating=rating,
                    comment=comment,
                    created_at=timezone.now()
                )
                review.save()

                review_serializer = ReviewSerializer(review)

                return JsonResponse({'message': 'review added successfully', 'review':review_serializer.data}, status=200)
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        else:
            return JsonResponse({'error': 'Method not allowed'}, status=405)
    else:
        return JsonResponse({'error': 'Unauthorized'}, status=401)
    
def delete_review(request, review_id):
    if not request.user.is_authenticated:
        return JsonResponse({'error': 'Unauthorized'}, status=401)
    if request.user==Review.objects.get(id=review_id).user:
        review = get_object_or_404(Review, id=review_id)
        if request.method == 'DELETE':
            review.delete()
            return JsonResponse({'message': 'Review deleted successfully!'})
        else:
            return JsonResponse({'message': 'Method not allowed'})
    else:
        return JsonResponse({'error': 'Unauthorized'}, status=401)
    
def update_review(request, review_id):
    if not request.user.is_authenticated:
        return JsonResponse({'error': 'Unauthorized'}, status=401)
    if request.user==Review.objects.get(id=review_id).user:
        review = get_object_or_404(Review, id=review_id)
        if request.method == 'PUT':
            data = json.loads(request.body)
            review.rating = data.get('rating', review.rating)
            review.comment = data.get('comment', review.comment)
            review.save()

            serializer = ReviewSerializer(review)
            return JsonResponse({'message': 'Review updated successfully!', 'review': serializer.data })
        else:
            return JsonResponse({'message': 'Method not allowed'})
    else:
        return JsonResponse({'error': 'Unauthorized'}, status=401)
    

def get_reviews(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    reviews = product.reviews.all()
    return JsonResponse({'reviews': [ReviewSerializer(review) for review in reviews]})

def get_reviews_by_user(request):
    if not request.user.is_authenticated:
        return JsonResponse({'error': 'Unauthorized'}, status=401)
    user = request.user
    reviews = user.review_set.all()  # Assuming this is the corrected line
    # Serialize each review along with product details
    serialized_reviews = []
    for review in reviews:
        review_data = ReviewSerializer(review).data
        product_data = ProductSerializer(review.product).data  # Assuming review has a product attribute
        review_data['product'] = product_data  # Add product details to review data
        serialized_reviews.append(review_data)
    return JsonResponse({'reviews': serialized_reviews})

def get_review_average(product_id):
    product = get_object_or_404(Product, id=product_id)
    reviews = product.reviews.all()
    total = 0
    for review in reviews:
        total += review.rating
    return total/len(reviews) if len(reviews) > 0 else 0


        
