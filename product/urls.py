from django.urls import path,include
import product.views as views 

urlpatterns = [
    path('getall/', views.get_all_products),  # List all products
    path('getall/<str:category>/', views.get_products_by_category),
    # path('detail/<str:product_id>/', views.get_product_detail),  # Get product details
    # path('create/', views.create_product),  # Create a product
    # path('update/<str:product_id>/', views.update_product),  # Update a product
    # path('delete/<str:product_id>/', views.delete_product),  # Delete a product

     path('', views.get_all_products),  # List all products
    path('detail/<str:product_id>/', views.get_product_detail),  # Get product details
    path('create/', views.create_product),  # Create a product
    path('update/<str:product_id>/', views.update_product),  # Update a product
    path('delete/<str:product_id>/', views.delete_product),  # Delete a product
    #reviews
    path('review/create/', views.add_review),  # Create a review
    path('review/delete/<int:review_id>/', views.delete_review),  # Delete a review
    path('review/update/<int:review_id>/', views.update_review),  # Update a review
    path('review/product/<str:product_id>/', views.get_reviews),  # Get reviews by product id
    path('review/user/', views.get_reviews_by_user),  # Get reviews by user
]