
from datetime import datetime
from django.contrib.auth import login, authenticate, logout
from django.http import JsonResponse
from django.contrib.auth.models import User
import json
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from .models import ShoppingCart, CartItem
from product.models import Product
from orders.models import Order, OrderItem, Sale
from .serializer import ShoppingCartSerializer, CartItemSerializer

@csrf_exempt
def cart(request):
    
    if request.method == 'GET':
        if request.user.is_authenticated:
            try:
                shopping_cart = ShoppingCart.objects.get(user=request.user)
                cart_items = CartItem.objects.filter(cart=shopping_cart)
                serializer = CartItemSerializer(cart_items, many=True)
                return JsonResponse({'data':serializer.data}, safe=False, status=200)  # Add safe=False here
            except ShoppingCart.DoesNotExist:
                return JsonResponse({'data': []}, status=200)
            
        else:
            return JsonResponse({'error':'Unauthorized'}, status=401)
    else:
        return JsonResponse({'error':'Method not allowed'}, status=405)
    


@csrf_exempt
def add_to_cart(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            try:
                shopping_cart = ShoppingCart.objects.get(user=request.user)
            except ShoppingCart.DoesNotExist:
                shopping_cart = ShoppingCart.objects.create(user=request.user)

            data = json.loads(request.body)
            product_id = data.get('product_id')
            quantity = data.get('quantity')

            product = Product.objects.get(id=product_id)
            try:
                cart_item = CartItem.objects.get(cart=shopping_cart, product=product)
                cart_item.quantity += quantity
           
                cart_item.save()
            except CartItem.DoesNotExist:
                cart_item = CartItem.objects.create(cart=shopping_cart, product=product)
            
            return JsonResponse({'message':'Product added to cart'}, status=200)
        else:
            return JsonResponse({'error':'Unauthorized'}, status=401)
    else:
        return JsonResponse({'error':'Method not allowed'}, status=405)
    
@csrf_exempt
def delete_from_cart(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            try:
                shopping_cart = ShoppingCart.objects.get(user=request.user)
            except ShoppingCart.DoesNotExist:
                return JsonResponse({'error':'No cart for that user'}, status=400)

            data = json.loads(request.body)
            product_id = data.get('product_id')

            product = Product.objects.get(id=product_id)
            try:
                cart_item = CartItem.objects.get(cart=shopping_cart, product=product)
                cart_item.delete()
                return JsonResponse({'message':'Product removed from cart'}, status=200)
            except CartItem.DoesNotExist:
                return JsonResponse({'error':'Product not found in cart'}, status=400)
        else:
            return JsonResponse({'error':'Unauthorized'}, status=401)
    else:
        return JsonResponse({'error':'Method not allowed'}, status=405)
    

    

@csrf_exempt
def update_cart_item2(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            try:
                data = json.loads(request.body)
                cart_items = data.get('data', [])

                for item in cart_items:
                    cart_item_id = item.get('id')
                    new_quantity = item.get('quantity')

                    try:
                        cart_item = CartItem.objects.get(id=cart_item_id, cart__user=request.user)
                        cart_item.quantity = new_quantity
                        cart_item.save()
                    except CartItem.DoesNotExist:
                        return JsonResponse({'error': f'CartItem with id {cart_item_id} does not exist'}, status=400)

                return JsonResponse({'message': 'Cart updated successfully'}, status=200)
            except Exception as e:
                return JsonResponse({'error': str(e)}, status=500)
        else:
            return JsonResponse({'error': 'Method not allowed'}, status=405)
    else:
        return JsonResponse({'error': 'Unauthorized'}, status=401)


@csrf_exempt
def confirm_cart(request):
  
    if request.method == 'POST':
        if request.user.is_authenticated:
            try:
                request_body = json.loads(request.body)
                shopping_cart = ShoppingCart.objects.get(user=request.user)
                cart_items = CartItem.objects.filter(cart=shopping_cart)
                total_price = sum(item.product.product_price * item.quantity for item in cart_items)

                valid_time=request_body.get('delivery_time').strip()
                final_delivery_time=datetime.strptime(valid_time, "%d-%m-%Y")

                order = Order.objects.create(
                    client=request.user,
                    total_price=total_price,
                    credit_card_number=request_body.get('credit_card_number'),  
                    delivery_location= request_body.get('delivery_location') ,
                    delivery_time=final_delivery_time,
                    order_status="pending"
                )

                for item in cart_items:
                    OrderItem.objects.create(order=order, product=item.product, quantity=item.quantity)

                for item in cart_items:
                    Sale.objects.create(
                        order=order,
                        product=item.product,
                        quantity=item.quantity,
                        sale_price=item.product.product_price*item.quantity,  # Assuming sale price matches product price
                    )
                for item in cart_items:
                    item.delete()
                shopping_cart.save()
                return JsonResponse({'message': 'Order confirmed successfully'})
            except (  User.DoesNotExist, ShoppingCart.DoesNotExist):
                return JsonResponse({'error': 'Invalid data or object not found'}, status=400)
        else:
            return JsonResponse({'error': 'Method not allowed'}, status=405)
    else:
        return JsonResponse({'error': 'Unauthorized'}, status=401)