from django.urls import path
from .views import cart, add_to_cart, delete_from_cart,update_cart_item2, confirm_cart

urlpatterns = [
    path('view/', cart, name='test'),
    path('add/', add_to_cart, name='add_to_cart'),
    path('delete/', delete_from_cart, name='delete_from_cart'),
    path('update/', update_cart_item2, name='update_cart_item2'),
    path('confirm/', confirm_cart, name='confirm_cart')
]