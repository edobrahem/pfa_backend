from django.urls import path
from .views import getClientOrders, getOrder, getOrders, updateOrderStatus, topSoldProducts,  saleGraph, getClientOrdersDetails

urlpatterns = [
    path('clientOrders/', getClientOrders, name='getClientOrders'),
    path('clientOrdersDetails/', getClientOrdersDetails, name='getClientOrdersDetails'),
    path('all/', getOrders, name='getOrders'),
    path('<int:order_id>/', getOrder, name='getOrder'),
    path('update/<int:order_id>/',updateOrderStatus, name="updateOrderStatus"),
    path('topSold/', topSoldProducts, name="topSoldProducts"), 
    path('saleGraph/<int:product_id>',saleGraph, name="saleGraph")
]