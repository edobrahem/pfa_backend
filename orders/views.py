import json
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.db.models import Sum
from django.db.models import Sum, Expression
from datetime import date, datetime, timedelta
from django.utils import timezone
from orders.models import Order, OrderItem, Sale
from product.models import Product

def getOrders(request):
    if request.method == 'GET':
        try:
            orders = Order.objects.all()
            order_data = []

            for order in orders:
                order_data.append({
                    'order_id': order.id,
                    'total_order_price': order.total_price,
                    'order_status': order.order_status,
            })
            return JsonResponse({'orders': order_data})
        except (ValueError, User.DoesNotExist):
            return JsonResponse({'error': 'Invalid client ID or no orders found'}, status=400)
    else:
            return JsonResponse({'error': 'Method not allowed'}, status=405)

def getClientOrders(request):
    if request.method == 'GET':
        try:
            orders = Order.objects.filter(client=request.user)
            order_data = []

            for order in orders:
                order_data.append({
                    'order_id': order.id,
                    'total_order_price': order.total_price,
                    'order_status': order.order_status,
            })
            return JsonResponse({'orders': order_data})
        except (ValueError, User.DoesNotExist):
            return JsonResponse({'error': 'Invalid client ID or no orders found'}, status=400)
    else:
            return JsonResponse({'error': 'Method not allowed'}, status=405)

def updateOrderStatus(request, order_id):
    if request.method != 'POST':
        return JsonResponse({'error': 'Invalid request method'}, status=400)
    
    order_choices=["pending", "Pending",
        "confirmed", "Confirmed",
        "shipped", "Shipped",
        "delivered", "Delivered",
        "cancelled", "Cancelled",]

    try:
        order = get_object_or_404(Order, pk=order_id)
        
        new_status = json.loads(request.body).get('new_status')
        
        if new_status not in order_choices:  
            return JsonResponse({'error': 'Invalid order status'}, status=400)
        order.order_status = new_status
        order.save()

        return JsonResponse({'message': 'Order status updated successfully'})
    except (ValueError, Order.DoesNotExist):
        return JsonResponse({'error': 'Invalid order ID or data'}, status=400)
        

def getOrder(request, order_id):
    if request.method == 'GET':
        try:
            order = get_object_or_404(Order, pk=order_id)
            order_data = {
            'order_id': order.id,
            'total_order_price': order.total_price,
            'order_status': order.order_status,
            'items': [],
            }
            for item in OrderItem.objects.filter(order=order):
                
                order_data['items'].append({
                    'product_id': item.product.id,
                    'product_name': item.product.product_name,  # Assuming product model has a 'name' field
                    'quantity': item.quantity,
                    'sale_price': item.product.product_price * item.quantity,  # Calculate total price per item
                })

            return JsonResponse(order_data)
        except Order.DoesNotExist:
            return JsonResponse({'error': 'Invalid order ID'}, status=400)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)

def topSoldProducts(request):
  if request.method == 'GET':
    try:
        top_products = (
            Sale.objects.values('product_id')
            .annotate(total_sold=Sum('quantity'))
            .order_by('-total_sold')[:5]
        )

        for product in top_products:
            product_object=get_object_or_404(Product, pk=product['product_id'])
            product['product_name']=product_object.product_name
            product['product_brand']=product_object.product_brand
            product['total_profit'] = product['total_sold'] * Sale.objects.filter(product=product['product_id']).values('sale_price').first()['sale_price']

        return JsonResponse(list(top_products), safe=False)  
    except(ValueError, Sale.DoesNotExist, Product.DoesNotExist):
        return JsonResponse({'error': 'Missing object(s) in database'}, status=400)
  else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)

def saleGraph(request, product_id):
    if request.method == 'GET':
        try:
            past_year = timezone.now() - timedelta(days=365)
            monthly_data = Sale.objects.filter(
                product_id=product_id,
                sale_date__gte=past_year,
            ).values('product_id', 'sale_date__month').annotate(total_quantity=Sum('quantity')).order_by('sale_date__month')

            response_data = []
            month_names = ['January','February','March', 
                'April','May','June','July','August','September','October','November','December']
            
            for month_data in monthly_data:
                response_data.append({
                'month': month_names[month_data['sale_date__month']-1],
                'total_sale_quantity': month_data['total_quantity']
                })
            return JsonResponse(list(response_data), safe=False)
        
        except(ValueError, Sale.DoesNotExist, Product.DoesNotExist):
            return JsonResponse({'error': 'Missing object(s) in database'}, status=400)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)
    
def getClientOrdersDetails(request):
    if request.method == 'GET':
        try:
            orders = Order.objects.filter(client=request.user)
            order_data = []

            for order in orders:
                order_data.append({
                    'order_id': order.id,
                    'total_order_price': order.total_price,
                    'order_status': order.order_status,
                    'items': [],
                })
                for item in OrderItem.objects.filter(order=order):
                    order_data[-1]['items'].append({
                        'product_id': item.product.id,
                        'product_name': item.product.product_name,  # Assuming product model has a 'name' field
                        'quantity': item.quantity,
                        'sale_price': item.product.product_price * item.quantity,  # Calculate total price per item
                    })

            return JsonResponse({'orders': order_data})
        except Order.DoesNotExist:
            return JsonResponse({'error': 'Invalid order ID'}, status=400)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)